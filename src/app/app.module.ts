import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HomeComponent } from './components/home/home.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AboutComponent } from './components/about/about.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SpotifyApiService } from './service/spotify-api.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { UtilService } from './service/util.service';
import { ArtistsComponent } from './components/artists/artists.component';
import { ArtistComponent } from './components/artist/artist.component';
import { AlbumComponent } from './components/album/album.component';
import { AsyncLocalStorageModule } from 'angular-async-local-storage';
import {WebStorageModule, LocalStorageService} from "angular-localstorage";
import { TruncateModule } from 'ng2-truncate';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { SettingService } from './service/settings.service';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  {
    path: 'settings',
    component: SettingsComponent,
  },
  {
    path: 'about',
    component: AboutComponent,
  },
  {
    path: 'artist/:id',
    component: ArtistComponent,
  },
  {
    path: 'album/:id',
    component: AlbumComponent,
  },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

/**
 * load i18n file
 * @param {HttpClient} http
 * @returns {TranslateHttpLoader}
 */
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}



@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    SettingsComponent,
    AboutComponent,
    PageNotFoundComponent,
    ArtistsComponent,
    ArtistComponent,
    AlbumComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AsyncLocalStorageModule,
    TruncateModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [SpotifyApiService, UtilService, SettingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
