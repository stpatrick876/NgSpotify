import { Injectable } from '@angular/core';

@Injectable()
export class UtilService {

  constructor() { }

   getJsonFromUrl(fragment) {
     const result = {};
     fragment.split('&').forEach((part) => {
      const item = part.split('=');
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
  }

}
