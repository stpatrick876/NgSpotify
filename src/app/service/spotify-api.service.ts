// angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// rxjs
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/of';

import { SpotifyAuthorizationResponse } from '../common.interface';
import { AsyncLocalStorage } from 'angular-async-local-storage';
import { isNullOrUndefined } from 'util';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SpotifyApiService {
  searchUrl: string;
  accessToken: string;


  constructor(private _http: HttpClient, protected _localStorage: AsyncLocalStorage) {
  }


  searchMusic(srcString: string, type = 'artist') {
   return  Observable.of(localStorage.getItem('spotifyAuthorizationResponse'))
     .map(item => JSON.parse(item))
     .filter(spotifyAuthRes => !isNullOrUndefined(spotifyAuthRes) && !isNullOrUndefined(spotifyAuthRes.access_token))
     .mergeMap( (spotifyAuthRes: SpotifyAuthorizationResponse) => {
      return this._http.get(`https://api.spotify.com/v1/search?query=${srcString}&offset=0&limit=20&type=${type}&market=US&access_token=${spotifyAuthRes.access_token}`);
    });
  }


  getArtist(id: string) {
    return Observable.of(localStorage.getItem('spotifyAuthorizationResponse'))
      .map(item => JSON.parse(item))
      .filter(spotifyAuthRes => !isNullOrUndefined(spotifyAuthRes) && !isNullOrUndefined(spotifyAuthRes.access_token))
      .mergeMap( (spotifyAuthRes: SpotifyAuthorizationResponse) => {
      const header = new HttpHeaders({
        Authorization: `Bearer ${spotifyAuthRes.access_token}`
      });
      return this._http.get(`https://api.spotify.com/v1/artists/${id}`, {headers: header});
    });
  }


  getAlbums(artistId: string) {
    return Observable.of(localStorage.getItem('spotifyAuthorizationResponse'))
      .map(item => JSON.parse(item))
      .filter(spotifyAuthRes => !isNullOrUndefined(spotifyAuthRes) && !isNullOrUndefined(spotifyAuthRes.access_token))
      .mergeMap( (spotifyAuthRes: SpotifyAuthorizationResponse) => {
      const header = new HttpHeaders({
        Authorization: `Bearer ${spotifyAuthRes.access_token}`
      });
      return this._http.get(`https://api.spotify.com/v1/artists/${artistId}/albums`, {headers: header});
    });
  }

  getAlbum(albumId: string) {
    return  Observable.of(localStorage.getItem('spotifyAuthorizationResponse'))
      .map(item => JSON.parse(item))
      .filter(spotifyAuthRes => !isNullOrUndefined(spotifyAuthRes) && !isNullOrUndefined(spotifyAuthRes.access_token))
      .mergeMap( (spotifyAuthRes: SpotifyAuthorizationResponse) => {
      const header = new HttpHeaders({
        Authorization: `Bearer ${spotifyAuthRes.access_token}`
      });
      return this._http.get(`https://api.spotify.com/v1/albums/${albumId}`, {headers: header});
    });
  }

  getUser() {
    return  Observable.of(localStorage.getItem('spotifyAuthorizationResponse'))
      .map(item => JSON.parse(item))
      .filter(spotifyAuthRes => !isNullOrUndefined(spotifyAuthRes) && !isNullOrUndefined(spotifyAuthRes.access_token))
      .mergeMap( (spotifyAuthRes: SpotifyAuthorizationResponse) => {
        const header = new HttpHeaders({
          Authorization: `Bearer ${spotifyAuthRes.access_token}`
        });
        return this._http.get(`https://api.spotify.com/v1/me`, {headers: header});
      });

  }

  deAuthorize() {
    localStorage.removeItem('spotifyAuthorizationResponse');
    window.location.reload();
  }


}
