import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UtilService } from './service/util.service';
import { Setting, SpotifyAuthorizationResponse, Theme } from './common.interface';
import { isNullOrUndefined } from 'util';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { SettingService } from './service/settings.service';

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  spotifyAuthResponse: SpotifyAuthorizationResponse;
  authorized: boolean;
  @HostBinding('class.light-theme') isLightTheme = false;
  @HostBinding('class.dark-theme') isDarkTheme = false;
  @HostBinding('class.spotify-theme') isSpotifyTheme = false;

  constructor(private _activatedRoute: ActivatedRoute, private _util: UtilService,private _translate: TranslateService, private _settingSrv: SettingService) {
    _translate.setDefaultLang('en');

    _settingSrv.changeEmitted$.subscribe((setting: Setting) => {
      localStorage.setItem('app_lang', setting.language.toString());
      _translate.use(setting.language.toString());

          switch (setting.theme) {
            case Theme.light:
              this.isDarkTheme = false;
              this.isLightTheme = true;
              this.isLightTheme = false;
              localStorage.setItem('app_theme', setting.theme);
              break;
            case Theme.dark:
              this.isDarkTheme = true;
              this.isLightTheme = false;
              this.isSpotifyTheme = false;
              localStorage.setItem('app_theme', setting.theme);
              break;
            case Theme.spotify:
              this.isSpotifyTheme = true;
              this.isDarkTheme = false;
              this.isLightTheme = false;
              localStorage.setItem('app_theme', setting.theme);
              break;
            default:
          }
    });
  }

  ngOnInit() {
    Observable.of(localStorage.getItem('app_lang')).subscribe(lang => {
      this._translate.use(lang);

    });

      Observable.of(localStorage.getItem('app_theme')).subscribe(theme => {
      switch (theme) {
        case Theme.light:
          this.isDarkTheme = false;
          this.isLightTheme = true;
          this.isSpotifyTheme = false;
          break;
        case Theme.dark:
          this.isDarkTheme = true;
          this.isLightTheme = false;
          this.isSpotifyTheme = false;
          break;
        case Theme.spotify:
          this.isDarkTheme = false;
          this.isLightTheme = false;
          this.isSpotifyTheme = true;

          break;
        default:
      }
    });

    this.isAuthorized();

    this._activatedRoute.fragment.subscribe((fragment: string) => {
      if (fragment) {
        this.spotifyAuthResponse = this._util.getJsonFromUrl(fragment) as SpotifyAuthorizationResponse;
        this.spotifyAuthResponse.expires_at =  Number(new Date()) + Number(this.spotifyAuthResponse.expires_in);

        localStorage.setItem('spotifyAuthorizationResponse', JSON.stringify(this.spotifyAuthResponse));
        window.location.hash = '';

        this.isAuthorized();
      }
    });

  }

  isAuthorized() {
    Observable.of(localStorage.getItem('spotifyAuthorizationResponse'))
      .map(item => JSON.parse(item))
      .filter(spotifyAuthRes => !isNullOrUndefined(spotifyAuthRes) && !isNullOrUndefined(spotifyAuthRes.access_token))
      .subscribe(res => {
        this.authorized = true;
      });
  }


}
