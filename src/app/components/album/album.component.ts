import { Component, OnInit } from '@angular/core';
import { Album } from '../../common.interface';
import { ActivatedRoute } from '@angular/router';
import { SpotifyApiService } from '../../service/spotify-api.service';

@Component({
  selector: 'album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {
  album: Album;

  constructor(private _route: ActivatedRoute, private _api: SpotifyApiService) { }

  ngOnInit() {
    this._route.params
      .map(params => params['id'])
      .subscribe(id => {

        this._api.getAlbum(id).subscribe(album => this.album = album as Album);

      });
  }

}
