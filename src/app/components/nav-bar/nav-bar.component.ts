import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SpotifyApiService } from '../../service/spotify-api.service';
import { SpotifyAuthorizationResponse } from "../../common.interface";

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  navItems: any;
  user: any;

  constructor(private _router: Router, private _api: SpotifyApiService) { }

  ngOnInit() {
    this.navItems = [{
      text: 'COMMON.SEARCH',
      icon: 'search',
      path: 'home'
    }, {
      text: 'SETTING.PAGE_TITLE',
      icon: 'settings',
      path: 'settings'
    }, {
      text: 'COMMON.ABOUT',
      icon: 'info',
      path: 'about'
    }];
    this.getUser();
  }

  isAuthorized() {
    const authData = JSON.parse(localStorage.getItem('spotifyAuthorizationResponse')) as SpotifyAuthorizationResponse;
   /* if(authData) {
      this.getUser();
    }*/
    return authData;
  }

  deAuthorize() {
   this._api.deAuthorize();
  }

  getUser() {
    this._api.getUser().subscribe(user => {
      this.user = user;
    });
  }

}
