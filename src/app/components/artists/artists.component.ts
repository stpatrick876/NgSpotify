import { Component, Input, OnInit } from '@angular/core';
import { Artist } from '../../common.interface';

@Component({
  selector: 'artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.scss']
})
export class ArtistsComponent implements OnInit {
  @Input() artists: Artist[];

  constructor() { }

  ngOnInit() {
  }

}
