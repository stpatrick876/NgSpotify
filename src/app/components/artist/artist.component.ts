import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyApiService } from '../../service/spotify-api.service';
import { Album, Artist } from '../../common.interface';
import 'rxjs/add/operator/map';

@Component({
  selector: 'artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent implements OnInit {
  id: string;
  artist: Artist;
  albums: Album[];
  constructor(private _route: ActivatedRoute, private _api: SpotifyApiService) { }

  ngOnInit() {
    this._route.params
      .map(params => params['id'])
      .subscribe(id => {
          this._api.getArtist(id).subscribe(artist => this.artist = artist  as Artist);

        this._api.getAlbums(id).subscribe((albums: any) => this.albums = albums.items  as Album[]);

    });
  }

}
