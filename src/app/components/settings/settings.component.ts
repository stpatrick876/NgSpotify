import { Component, OnInit } from '@angular/core';
import { SettingService } from '../../service/settings.service';
import { Language, Setting, Theme } from '../../common.interface';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  settings: Setting = {
    language: Language.en,
    theme: Theme.light
  };
  language = Language;
  constructor(private _settingSrv: SettingService) { }

  ngOnInit() {
    Observable.of(localStorage.getItem('app_lang')).subscribe(lang => {
       this.settings.language = Language[lang];
       console.log(this.settings);
    });

    Observable.of(localStorage.getItem('app_theme')).subscribe(theme => {

    });
  }

  updateSettings() {
    this._settingSrv.emitChange(this.settings);
  }

}
