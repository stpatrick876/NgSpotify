import { Component, OnInit } from '@angular/core';
import { SpotifyApiService } from '../../service/spotify-api.service';
import { ActivatedRoute } from '@angular/router';
import { Artist, SpotifyAuthorizationResponse } from '../../common.interface';
import { Observable } from 'rxjs/Observable';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  searchInput: string;
  searchResults: Artist[];
  authorized: boolean;

  constructor(private _api: SpotifyApiService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.isAuthorized();
  }


  searchMusic() {
    const authData = JSON.parse(localStorage.getItem('spotifyAuthorizationResponse')) as SpotifyAuthorizationResponse;

    if (Number(new Date()) > authData.expires_at) {
      this._api.searchMusic(this.searchInput).subscribe((res: any) => {
        this.searchResults = res.artists.items;
      });
    } else {
      this._api.deAuthorize();

    }

  }

  isAuthorized() {
    Observable.of(localStorage.getItem('spotifyAuthorizationResponse'))
      .map(item => JSON.parse(item))
      .filter(spotifyAuthRes => !isNullOrUndefined(spotifyAuthRes) && !isNullOrUndefined(spotifyAuthRes.access_token))
      .subscribe(res => {
        this.authorized = true;
      });
  }

  get spotifyAuthorizationUrl () {
    const params = {
      client_id: 'efdbac2cbe134ab7b8ef24feead4e4a7',
      response_type: 'token',
      redirect_uri: 'http://localhost:4200/home'
    };

    const esc = encodeURIComponent;
    const query = Object.keys(params)
      .map(k => `${esc(k)}=${esc(params[k])}`)
      .join('&');

    return `https://accounts.spotify.com/authorize?${query}`;
  }

  goToAuthorizationPage() {
    window.location.href = this.spotifyAuthorizationUrl;
  }

}
